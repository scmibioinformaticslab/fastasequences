class FastaSequences:
	# ==== CLASS PROPERTIES ====
	# @sequenceList[list] - list of sequences in case that user wants to add the sequence by themselves
	# @sequenceCount[int] - an integer containing number of total sequences in the file
	sequenceList = []
	sequenceCount = 0

	# ==== CONSTRUCTOR ====
	# initiate the class by
	# 1. use the user-inputed list
	# 2. read from a file
	# ==== PARAMETERS ====
	# @sequenceList[list] - list of sequences in case that user wants to add the sequence by themselves
	# @fileFullPath[string] - full path (or relative path) with file name
	#				  in case that user wants to read sequences from the file
	def __init__(self, sequenceList = False, fileFullPath = False):
		# clear up data just in case the memory is not cleaned up
		self.sequenceCount = 0
		self.sequenceList = []

		# read data from file if the sequenceList is not specified
		if(sequenceList != False or type(sequenceList).__name__ != 'list'):
			self.readFromFile(fileFullPath)
		else:
			self.sequenceList = sequenceList

	# ==== readFromFile METHOD ====
	# read sequences from file and stored in sequenceList
	# each sequence is stored in a dictionary with the following information
	# @gi[string] - contain gene id
	# @ref[string] - contain reference
	# @description[string] - contain description of the sequence
	# @sequence[string] - contain proteins or nucleotide bases
	# ==== PARAMETERS ====
	# @fileFullPath[string] - full path (or relative path) with file name
	def readFromFile(self, fileFullPath):
		# ---- VARIABLE EXPLANATION ----
		# @input_file - file object of the opened input fasta file
		# @handle - temporary variable to hold the whole file data
		# @sequence_list - list containing information of the file
		#				   an element contain text information of a sequence
		with open(fileFullPath, 'r') as input_file:
			handle = input_file.read()
			# seperate file content into sections- a section contain information of 1 sequence
			# since we use splitting to do this, the first element of the list will always contain 
			# 	empty information - so we need list slicing
			sequence_list = handle.split('>')[1:]
			self.sequenceCount = len(sequence_list)

			for a_sequence in sequence_list:
				# ---- VARIABLE EXPLANATION ----
				# @a_sequence - a string containing a chunck of information for a sequence
				# @header_end_index - the end point of the header section of a sequence in reference to a_sequence
				# @header - a list of string - each element contain string of each header block[ block is seperated by | ]
				# @gi - a variable use to hold information of gi as contained in sequence header
				# @ref - a variable use to hold information of ref as contained in sequence header
				# @description - a variable use to hold information with no label
				# @sequence - a variable use to hold the sequence itself
				# @sequence_dict - a temporary dictionary holding information of 1 sequence
				header_end_index = a_sequence.find('\n')
				header = a_sequence[0 : header_end_index]
				header = header.split('|')
				gi = ''
				ref = ''
				description = ''
				# loop through the header list checking which information the header contain
				# and keep gi/ref/description information into the temporary variable
				for i in range(len(header)):
					if header[i].lower() == 'gi':
						gi = header[i+1]
					elif header[i].lower() == 'ref':
						ref = header[i+1]
					elif header[i-1].lower() != 'gi' and header[i-1].lower() != 'ref':
						description = header[i]
				sequence = a_sequence[header_end_index + 1 : len(a_sequence)].strip()
				sequence = sequence.replace('\n', '')
				sequence = sequence.replace('\r', '')
				sequence_dict = { 'gi': gi, 
								  'ref': ref, 
								  'description': description, 
								  'sequence': sequence}
				self.sequenceList.append(sequence_dict)

	# ==== getGeneInfoFromOrder FUNCTION ====
	# return a dictionary contain sequence information from sequenceList
	# ==== PARAMATERS ====
	# @index[int] - order of the gene within a file - it is also list index
	def getGeneInfoFromOrder(self, index):

		return sequenceList[index]

	# ==== getSequenceFromGI METHOD ====
	# return the sequence of specific gene from the gene id
	# ==== PARAMETERS ====
	# @gi[string] - gene id to obtain the sequence from
	def getSequenceFromGI(self, gi):
		for each in self.sequenceList:
			if each['gi'].lower() == gi.lower():
				return each['sequence']
		else:
			return False

	# ==== getSequenceFromRef METHOD ====
	# return the sequence of specific gene from the ref
	# ==== PARAMETERS ====
	# @ref[string] - ref to obtain the sequence from
	def getSequenceFromRef(self, ref):
		for each in self.sequenceList:
			if each['ref'].lower() == ref.lower():
				return each['sequence']
		else:
			return False

	# ==== getSequenceFromRef METHOD ====
	# return the sequence of specific gene from the description
	# ==== PARAMETERS ====
	# @description[string] - description to obtain the sequence from
	def getSequenceFromDescription(self, description):
		for each in self.sequenceList:
			if each['description'].lower() == description.lower():
				return each['sequence']
		else:
			return False

	# ==== getDescriptionFromGI METHOD ====
	# return the description of specific gene from the gene id
	# ==== PARAMETERS ====
	# @gi[string] - gene id to obtain the description from
	def getDescriptionFromGI(self, gi):
		for each in self.sequenceList:
			if each['gi'].lower() == gi.lower():
				return each['description']
		else:
			return False

	# ==== getDescriptionFromGI METHOD ====
	# return the description of specific gene from the ref
	# ==== PARAMETERS ====
	# @ref[string] - ref to obtain the description from
	def getDescriptionFromRef(self, ref):
		for each in self.sequenceList:
			if each['ref'].lower() == ref.lower():
				return each['description']
		else:
			return False

	# ==== writeFasta METHOD ====
	# write fasta file from the sequences in the class
	# ==== PARAMETERS ====
	# @fileFullPath[string] - full path (or relative path) with file name
	# @headerList[list] - list of information to write to the sequence name part
	# @filteredList[list] - list of index of sequence to be write to file
	def writeFasta(self, fileFullPath, headerList, filteredList = []):
		# ---- VARIABLE EXPLANATION ----
		# @output_fasta - file object of the opened output fasta file
		with open(fileFullPath, 'w') as output_fasta:
			for a_sequence in self.sequenceList:
				if self.sequenceList.index(a_sequence) not in filteredList:
					# ---- VARIABLE EXPLANATION ----
					# @a_sequence - one sequence dictionary from sequenceList
					# @output_fasta_tmp - temporary string containing information of one sequence 
					#					  that will be write into the output file
					output_fasta_tmp = '>'
					for an_info in headerList:
						# ---- VARIABLE EXPLANATION ----
						# @an_info - a string from headerList containing the key of information 
						#			 to be write to the header section of the sequence
						if(an_info in a_sequence):
							output_fasta_tmp += '%s|%s|' % (an_info, a_sequence[an_info])
					output_fasta_tmp += '\n%s\n' % (a_sequence['sequence'])
					output_fasta.write(output_fasta_tmp)

	# ==== countSequencesFilteredByLength METHOD ====
	# count number of sequences that have certain range of length
	# ==== PARAMETERS ====
	# @minLength[int] - lowerbound length of sequence to be count
	# @maxLength[int] - upperbound length of sequence to be count
	#				    if not specified sequence with length to INFINITY will be included
	def countSequencesFilteredByLength(self, minLength, maxLength = False):
		# ---- VARIABLE EXPLANATION ----
		# @filtered_list - list of sequences that pass filter criteria
		if maxLength == 'INFINITY' or maxLength == False:
			filtered_list = [f for f in self.sequenceList if len(f['sequence']) >= minLength]
		else:
			filtered_list = [f for f in self.sequenceList if len(f['sequence']) >= minLength and len(f['sequence']) <= maxLength]
		return len(filtered_list)

	# ==== cutSequenceByRegion METHOD ====
	# cut specified region of all sequence and return as list
	# ==== PARAMETERS ====
	# @firstPosition[int] - first position to include in the sequence
	# @lastPosition[int] - last position to include in the sequence
	# @filteredList[list] - list of index of sequence not to be cut
	# @aSequence[string] - sequence to cut
	def cutSequenceByRegion(self, firstPosition, lastPosition, filteredList = [], aSequence = False):
		# ---- VARIABLE EXPLANATION ----
		# @a_sequence - a sequence dictionary in the list
		if aSequence == False:
			return [ a_sequence['sequence'][firstPosition:lastPosition+1] \
					 for a_sequence in self.sequenceList \
					 if self.sequenceList.index(a_sequence) not in filteredList
				   ]
		else:
			return aSequence[ firstPosition : lastPosition+1 ]

	# ==== findSequence METHOD ====
	# find the specified sequence in the @sequenceList
	# ==== PARAMETERS ====
	# @sequence[string] - finding sequence
	# @returnValue[string] - value to return upon finding the sequence. Can be:
	#				 			1. 'bool'
	#							2. 'gi'
	#							3. 'ref'
	#							4. 'description'
	#				 			5. 'sequence'
	def findSequence(self, sequence, returnValue):
		found_index = []
		for index, a_sequence in enumerate(self.sequenceList):
			upper_sequence = a_sequence['sequence'].upper()
			sequence = sequence.upper()
			if upper_sequence.find(sequence) != -1:
				found_index.append(index)

		if len(found_index) > 0:
			if returnValue == 'bool':
				return True
			else:
				return_list = []
				for an_index in found_index:
					return_list.append(self.sequenceList[an_index][returnValue])
				return return_list
		else:
			return False


	# ==== complementSequence METHOD ====
	# reverse complement the input sequence
	# ==== PARAMETERS ====
	# @sequence[string] - sequence to complement
	# @reverse[boolean] - whether to reverse the sequence or not
	def complementSequence(self, sequence, reverse = True):
		# ==== LOCAL VARIABLES ====
		# @complement_table[dictionary] - Table to use in complementing sequence
		complement_table = {
							'A': 'T', 'T': 'A', 'C': 'G', 'G': 'C',
							'a': 't', 't': 'a', 'c': 'g', 'g': 'c',
							'Y': 'R', 'R': 'Y', 'y': 'r', 'r': 'y',
							'K': 'M', 'M': 'K', 'k': 'm', 'm': 'k',
							'B': 'V', 'V': 'B', 'D': 'H', 'H': 'D',
							'b': 'v', 'v': 'b', 'd': 'h', 'h': 'd'
							}

		# Reverse the given sequence
		if reverse == True:
			sequence = sequence[::-1]

		sequence = list(sequence)

		for base_index in range(len(sequence)):
			base = sequence[base_index]
			sequence[base_index] = complement_table.get(base, 'N')

		sequence = ''.join(sequence)
		return sequence


	# ==== translateToAminoAcid METHOD ====
	# translate the input nucleotide sequence to amino acid sequence
	# ==== PARAMETERS ====
	# @sequence[string] - sequence to translate to amino acid
	# @readingFrame[int] - reading frame to translate DNA to Amino Acid
	#					   can be [+1,+2,+3,-1,-2,-3]
	def translateToAminoAcid(self, sequence, readingFrame = +1):
		# ==== LOCAL VARIABLES ====
		# @gencode[dictionary] - A dictionary containing table for amino acid translation
		# @translate_sequence[string] - A DNA sequence to be translate
		# @translated_aa_sequence[string] - Amino Acid sequence translated
		gencode = {
					'ATA':'I', 'ATC':'I', 'ATT':'I', 'ATG':'M',
					'ACN':'T',
					'ACA':'T', 'ACC':'T', 'ACG':'T', 'ACT':'T',
					'AAC':'N', 'AAT':'N', 'AAA':'K', 'AAG':'K',
					'AGC':'S', 'AGT':'S', 'AGA':'R', 'AGG':'R',
					'CTN':'L',
					'CTA':'L', 'CTC':'L', 'CTG':'L', 'CTT':'L',
					'CCN':'P',
					'CCA':'P', 'CCC':'P', 'CCG':'P', 'CCT':'P',
					'CAC':'H', 'CAT':'H', 'CAA':'Q', 'CAG':'Q',
					'CGN':'R',
					'CGA':'R', 'CGC':'R', 'CGG':'R', 'CGT':'R',
					'GTN':'V',
					'GTA':'V', 'GTC':'V', 'GTG':'V', 'GTT':'V',
					'GCN':'A',
					'GCA':'A', 'GCC':'A', 'GCG':'A', 'GCT':'A',
					'GAC':'D', 'GAT':'D', 'GAA':'E', 'GAG':'E',
					'GGN':'G',
					'GGA':'G', 'GGC':'G', 'GGG':'G', 'GGT':'G',
					'TCN':'S',
					'TCA':'S', 'TCC':'S', 'TCG':'S', 'TCT':'S',
					'TTC':'F', 'TTT':'F', 'TTA':'L', 'TTG':'L',
					'TAC':'Y', 'TAT':'Y', 'TAA':'*', 'TAG':'*',
					'TGC':'C', 'TGT':'C', 'TGA':'*', 'TGG':'W'}

		# Make all sequence uppercase first
		sequence = sequence.upper()

		# Cut DNA to be translated according to reading frame
		if readingFrame == +1:
			translate_sequence = sequence[0:]
		elif readingFrame == +2:
			translate_sequence = sequence[1:]
		elif readingFrame == +3:
			translate_sequence = sequence[2:]
		elif readingFrame == -1:
			translate_sequence = sequence[::-1][0:]
		elif readingFrame == -2:
			translate_sequence = sequence[::-1][1:]
		elif readingFrame == -3:
			translate_sequence = sequence[::-1][2:]
		else:
			print "Invalid Reading Frame, using +1 frame instead."
			translate_sequence = sequence[0:]

		# Translate the sequence using @gencode dictionary
		# If no translation are available, using 'X' as a translation
		translated_aa_sequence = ''.join([gencode.get(translate_sequence[3*i:3*i+3],'X') for i in range(len(sequence)/3)])
		return translated_aa_sequence


	# ==== writeToAlignmentView METHOD ====
	# write all sequences to the new file so that the sequence can be compared
	# (for fasta file with aligned sequences inside)
	# ==== PARAMETERS ====
	# @outputName[string] - name of the output file
	def writeToAlignmentView(self, outputName):
		sequence_length = len(self.sequenceList[0]['sequence'])
		with open(outputName, 'w') as output_file:
			for i in range(0, sequence_length - 1, 60):
				if i + 60 < sequence_length:
					end_line = i + 60
				else:
					end_line = sequence_length - 1

				for j in range(self.sequenceCount):
					output_file.write('seq%d\t\t%s\n' % (j, self.sequenceList[j]['sequence'][i:end_line]))

				output_file.write('\n\n')





if __name__ == '__main__':
	pass
	# input_file = '/vagrant/stretcher.align'

	# fasta = FastaSequences(False, input_file)
	# fasta.writeToAlignmentView('align_out.alignview')